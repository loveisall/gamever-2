#ifndef PLAYERONE_H
#define PLAYERONE_H
#include <SFML/Graphics.hpp>
#include "Gameobject.h"
class PlayerOne:public Gameobject {
public:
	float movementSpeed;
	int TotalCoins;
	int counterWalking;
	int direction;
	int lifes;
	sf::Time elapsed;
	PlayerOne();
	void update();
	void updateMovement();
};

#endif // !PLAYERONE_H

