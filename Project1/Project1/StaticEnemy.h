#ifndef  STATICENEMY_H
#define STATICENEMY_H

#include "Gameobject.h"
#include <SFML/Graphics.hpp>

class StaticEnemy : public Gameobject{
public:
	bool alive;
	StaticEnemy();
	void update();
};
#endif// STATICENEMY_H 

