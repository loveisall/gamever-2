#include "CreditScene.h"

CreditScene::CreditScene() {}

CreditScene::CreditScene(SfmlWindow *_creditwindow) {
	sfmlwind = _creditwindow;
	Start();
}

CreditScene::~CreditScene() {}

void CreditScene::Start() {
	std::cout << "CREDITS" << endl;
	SetFontTextAndBackGround();
}

void CreditScene::SetFontTextAndBackGround() {
	creditsBackgroundTexture.loadFromFile("Assets/Grilla.png");
	creditsbackgroundSprite.setTexture(creditsBackgroundTexture);
	creditsFont.loadFromFile("Assets/Long_Shot.ttf");
	text1.setFont(creditsFont);
	text2.setFont(creditsFont);
	text3.setFont(creditsFont);
	text4.setFont(creditsFont);
	text5.setFont(creditsFont);
}


void CreditScene::Draw() {
	//Background
	sfmlwind->window.draw(creditsbackgroundSprite);
	//Credit Text
	text1.setString("CREDITS");
	text1.setCharacterSize(20);
	text1.setOutlineColor(sf::Color::Blue);
	text1.setOutlineThickness(3);
	text1.setFillColor(sf::Color::White);
	text1.setPosition(sfmlwind->window.getSize().x / 2, 50);
	sfmlwind->window.draw(text1);

	text2.setString("THANKS FOR PLAYING");
	text2.setCharacterSize(20);
	text2.setOutlineColor(sf::Color::Blue);
	text2.setOutlineThickness(3);
	text2.setFillColor(sf::Color::White);
	text2.setPosition(sfmlwind->window.getSize().x / 2, sfmlwind->window.getSize().y-50);
	sfmlwind->window.draw(text2);

	text3.setString("Programming : Marcos Sosa");
	text3.setCharacterSize(20);
	text3.setOutlineColor(sf::Color::Blue);
	text3.setOutlineThickness(3);
	text3.setFillColor(sf::Color::White);
	text3.setPosition(sfmlwind->window.getSize().x / 2, sfmlwind->window.getSize().y-400);
	sfmlwind->window.draw(text3);



	text4.setString("2D Art : Marcos Sosa");
	text4.setCharacterSize(20);
	text4.setOutlineColor(sf::Color::Blue);
	text4.setOutlineThickness(3);
	text4.setFillColor(sf::Color::White);
	text4.setPosition(sfmlwind->window.getSize().x / 2, sfmlwind->window.getSize().y-350);
	sfmlwind->window.draw(text4);


	text5.setString("Music : Gabriel Aldaya");
	text5.setCharacterSize(20);
	text5.setOutlineColor(sf::Color::Blue);
	text5.setOutlineThickness(3);
	text5.setFillColor(sf::Color::White);
	text5.setPosition(sfmlwind->window.getSize().x / 2, sfmlwind->window.getSize().y-300);
	sfmlwind->window.draw(text5);



	text5.setString("Software used for this game : Adobe Photoshop & Visual Studio 2015");
	text5.setCharacterSize(20);
	text5.setOutlineColor(sf::Color::Red);
	text5.setOutlineThickness(5);
	text5.setFillColor(sf::Color::White);
	text5.setPosition(sfmlwind->window.getSize().x / 2, sfmlwind->window.getSize().y - 250);
	sfmlwind->window.draw(text5);

}



int CreditScene::Update() {
	scene = 2;
	while (sfmlwind->window.pollEvent(sfmlwind->event)) {
		switch (sfmlwind->event.type) {
		case sf::Event::KeyPressed:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				scene = 0;
			}
			break;
		case sf::Event::Closed:
			sfmlwind->window.close();
			break;
		default:
			break;
		}
	}
		sfmlwind->window.clear(sf::Color::Red);
		Draw();

		sfmlwind->window.display();
		return scene;	
}