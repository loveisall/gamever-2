#ifndef  CREDITSCENE_H
#define CREDITSCENE_H

#include "Scene.h"
#include <SFML/Graphics.hpp>
#include <iostream>
class CreditScene :public Scene{
private:
	void Start() override;
	int Update() override;
public :
	int scene;
	sf::Text text1;//creditos
	sf::Text text2;//Gracias por jugar
	sf::Text text3;//Programacion
	sf::Text text4;//Arte
	sf::Text text5;//Sonido
	sf::Text text6;//software utilizado
	sf::Font creditsFont;
	//background
	sf::Texture creditsBackgroundTexture;
	sf::Sprite creditsbackgroundSprite;
	CreditScene();
	CreditScene(SfmlWindow *_creditwindow);
	~CreditScene();
	void Draw();
	void SetFontTextAndBackGround();
};
#endif // ! CREDITSCENE_H
