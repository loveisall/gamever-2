#include "GameScene.h"

GameScene::GameScene() {}

GameScene::GameScene(SfmlWindow *_gamewindow) {
	sfmlwind = _gamewindow;
	Start();
}

GameScene::~GameScene() {}

void GameScene::Start() {
	SetTextures();
	SetSprites();
	CreateDinamicEnemies();
	CreateStaticEnemies();
	CreateCoins();
	SetFonts();
	SetText();
	SetSounds();
	Draw();
	
}

void GameScene::CreateDinamicEnemies() {
	enemy1->sprite.setTexture(textureDinamicEnemy);
	for (int i = 0; i < MaxDinamicEnemyQuantity; i++) {
		enemy1->rect.setPosition(generateRandom(sfmlwind->window.getSize().x), generateRandom(sfmlwind->window.getSize().y));
		enemyArray.push_back(*enemy1);
	}
}

void GameScene::CreateStaticEnemies() {
	enemy2->sprite.setTexture(textureStaticEnemy);
	for (int i = 0; i < MaxStaticEnemyQuantity; i++) {
		enemy2->rect.setPosition(generateRandom(sfmlwind->window.getSize().x),generateRandom(sfmlwind->window.getSize().y));
		staticenemyArray.push_back(*enemy2);
	}
}

void GameScene::CreateCoins() {
	coin->sprite.setTexture(textureCoin);
	for (int i = 0; i < MaxCoinQuantity; i++) {
		coin->rect.setPosition(generateRandom(sfmlwind->window.getSize().x), generateRandom(sfmlwind->window.getSize().y));
		CoinArray.push_back(*coin);
	}
}

void GameScene::SetTextures() {
	//Texture player
	texturePlayer.loadFromFile("Assets/PlayerOneVer2.png");
	//Texture Dinamic Enemies
	textureDinamicEnemy.loadFromFile("Assets/EnemyVer2.png");
	//Texture Static Enemies
	textureStaticEnemy.loadFromFile("Assets/StaticEnemyVer2.png");
	//Coin Texture
	textureCoin.loadFromFile("Assets/CoinVer2.png");
	//Background Texture
	backgroundTexture.loadFromFile("Assets/Grilla.png");
	//Finish Line Texture
	finishTexture.loadFromFile("Assets/CirculoMeta.png");	
}


void GameScene::SetSprites() {
	//Sprite Player	
	player1.sprite.setTexture(texturePlayer);
	player1.sprite.setScale(0.25f,0.25f);
	//Sprite Background
	backgroundSprite.setTexture(backgroundTexture);
	//Sprite FinishLine
	finishSprite.setTexture(finishTexture);
	finishSprite.setPosition(1080, 520);

	
}
void GameScene::SetText(){
	//Life player Text
	text.setFont(font);
	text.setString("");
	text.setCharacterSize(25);
	text.setPosition(0, 0);
	//Coins Grabbed Text
	text2.setFont(font);
	text2.setString("");
	text2.setCharacterSize(25);
	text2.setPosition(100, 0);
}

void GameScene::SetFonts() {
	font.loadFromFile("Assets/Long_Shot.ttf");
}

void GameScene::SetSounds() {
	buff1.loadFromFile("Assets/collidewithenemy.ogg");
	buff2.loadFromFile("Assets/grabcoin.ogg");
	sound1.setBuffer(buff1);
	sound2.setBuffer(buff2);
}

void GameScene::SaveScore() {
	std::ofstream scorefile;
	scorefile.open("HighScore.txt");
	if (scorefile.is_open()) {
		scorefile << player1.TotalCoins << endl;
		scorefile.flush();
		scorefile.close();
	}
}
string GameScene::ReadScore() {
		ifstream readscorefile;
		string read;
		readscorefile.open("HighScore.txt");
		if (readscorefile.is_open()) {		
			std::getline(readscorefile, read);
			readscorefile.close();
		}
		return read;
}


void GameScene::Collisions() {
	//Player collides with coins
	counter = 0;
	for (iter3 = CoinArray.begin(); iter3 != CoinArray.end(); iter3++) {
		if (CoinArray[counter].rect.getGlobalBounds().intersects(player1.sprite.getGlobalBounds())) {
			player1.TotalCoins++;
			CoinArray[counter].destroycoin = true;
			sound2.play();
		}
		counter++;
	}

	//Player Collides with Static Enemies
	counter = 0;
	for (iter2 = staticenemyArray.begin(); iter2 != staticenemyArray.end(); iter2++) {
		if (staticenemyArray[counter].rect.getGlobalBounds().intersects(player1.sprite.getGlobalBounds())) {
			staticenemyArray[counter].alive = false;
			player1.lifes--;
			sound1.play();
		}
		counter++;
	}

	//player collides with dinamic enemy
	counter = 0;
	for (iter1 = enemyArray.begin(); iter1 != enemyArray.end(); iter1++) {
		if (enemyArray[counter].rect.getGlobalBounds().intersects(player1.sprite.getGlobalBounds())) {
			enemyArray[counter].alive = false;
			player1.lifes--;
			sound1.play();
		}
		counter++;
	}
	//PlayerWin (collides with finish line)
	if (player1.sprite.getGlobalBounds().intersects(finishSprite.getGlobalBounds())) {
		scene = 2;
		SaveScore();
		ReadScore();
	}
}


void GameScene::DeletingCollision() {
	//Delete of coins
	counter = 0;
	for (iter3 = CoinArray.begin(); iter3 != CoinArray.end(); iter3++) {
		if (CoinArray[counter].destroycoin == true) {
			CoinArray.erase(iter3);
			delete coin;
			coin = NULL;
			break;
		}
		counter++;
	}
	//Delete of static enemies
	counter = 0;
	for (iter2 = staticenemyArray.begin(); iter2 != staticenemyArray.end(); iter2++) {
		if (staticenemyArray[counter].alive == false) {
			staticenemyArray.erase(iter2);
			delete enemy2;
			enemy2 = NULL;
			break;
		}
		counter++;
	}
	//Delete of dinamic enemies
	counter = 0;
	for (iter1 = enemyArray.begin(); iter1 != enemyArray.end(); iter1++) {
		if (enemyArray[counter].alive == false) {
			enemyArray.erase(iter1);
			delete enemy1;
			enemy1 = NULL;
			break;
		}
		counter++;
	}
}

void GameScene::KillPlayer() {
	if (player1.lifes <= 0) {
		scene = 2;
	}
}


void GameScene::Draw() {
	//Background Draw
	sfmlwind->window.draw(backgroundSprite);
	//Finish Line Draw
	sfmlwind->window.draw(finishSprite);
	//Coin Draw
	counter = 0;
	for (iter3 = CoinArray.begin(); iter3 != CoinArray.end(); iter3++) {
		CoinArray[counter].update();
		sfmlwind->window.draw(CoinArray[counter].sprite);
		counter++;
	}
	//Static Enemies draw
	counter = 0;
	for (iter2 = staticenemyArray.begin(); iter2 != staticenemyArray.end(); iter2++) {
		staticenemyArray[counter].update();
		sfmlwind->window.draw(staticenemyArray[counter].sprite);
		counter++;
	}
	//Dinamic Enemies draw
	counter = 0;
	for (iter1 = enemyArray.begin(); iter1 != enemyArray.end(); iter1++) {
		enemyArray[counter].update();
		enemyArray[counter].updateMovement();
		sfmlwind->window.draw(enemyArray[counter].sprite);
		counter++;
	}
	//Player One Draw
	player1.update();
	player1.updateMovement();
	sfmlwind->window.draw(player1.sprite);
}

void GameScene::drawText() {
	//In Game Text
	//Player Lifes
	text.setString("Lifes: " + to_string(player1.lifes));
	text.setOutlineColor(sf::Color::Blue);
	text.setOutlineThickness(3);
	sfmlwind->window.draw(text);
	//Coins Grabbed 
	text2.setString("Coins: " + to_string(player1.TotalCoins));
	sfmlwind->window.draw(text2);
}

int GameScene::Update() {
	scene = 1;
	//while (sfmlwind->window.isOpen())
	while (sfmlwind->window.pollEvent(sfmlwind->event)) {
		switch (sfmlwind->event.type){
		case sf::Event::KeyPressed:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				scene = 0;
			}
			//input?
			break;
		case sf::Event::Closed:
			sfmlwind->window.close();
			break;
		default:
			break;
		}
	}		
		//Collisions
		Collisions();
		//Delete Stuff player collided with
		DeletingCollision();
		//Kill player
		KillPlayer();
		//Draw
		Draw();
		//Draw in GameText
		drawText();		
		sfmlwind->window.display();
		return scene;
}



