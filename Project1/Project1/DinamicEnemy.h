#ifndef  DINAMICENEMY_H
#define DINAMICENEMY_H

#include "Gameobject.h"
#include "Rnd.h"
#include <SFML/Graphics.hpp>

class DinamicEnemy : public Gameobject{
public:
	float movementSpeed;
	int movementLength;
	int counterWalking;
	int direction; 
	int counter;
	bool alive;
	sf::Time elapsed;
	DinamicEnemy();
	void update();
	void updateMovement();
};
#endif// DINAMICENEMY_H 
