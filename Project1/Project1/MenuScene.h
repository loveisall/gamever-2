#ifndef MENUSCENE_H
#define MENUSCENE_H
//POCO STUFF
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>
#include<Poco\JSON\Parser.h>

//Includes
#include <SFML/Graphics.hpp>
#include <SFML\Audio\Music.hpp>
#include "Scene.h"
#include <iostream>
#include "HttpRequest.h"
#include <fstream>
#include "GameScene.h"
using namespace Poco;
using namespace Net;
class MenuScene:public Scene {
private :
	void Start() override;
	int Update() override;
	int numero;
	string weather;
	string scorefiletext;
public :
	//Http Request
	HttpRequest menuRequest;
	//All Menu Texts
	sf::Text text1;//Objetivo
	sf::Text text2;//Jugar
	sf::Text text3;//Creditos
	sf::Text text4;//Salir	
	sf::Text text5;//CLIMA
	sf::Text text6;//Score
	//Fonts
	sf::Font Menufont;
	//Music
	sf::Music menumusic;
	GameScene gamescene;
	//BackgroundTexture
	sf::Texture backgroundMenuTexture;
	sf::Sprite backgroundMenuSprite;
	MenuScene();
	MenuScene(SfmlWindow *_menuwindow);
	~MenuScene();
	void Draw();
	void SetFontTextandBackGround();
	void SetMusic();
	void ShowWeather();
};
#endif // !MENUSCENE_H

