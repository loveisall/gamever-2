#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "SfmlWindow.h"
#include "GameScene.h"
#include "MenuScene.h"
#include "CreditScene.h"
#include "Scene.h"
class SceneManager {
public :	
	SfmlWindow *scenewindow;
	GameScene *game;
	MenuScene *menu;
	CreditScene *credits;
	SceneManager();
	~SceneManager();
	enum Scenes {_menuScene,_gameScene,_creditsScene};
	int currentScene = _menuScene;
	void RunSceneManager();
	void GoToMenuScene();
	void GoToGameScene();
	void GoToCreditsScene();
};
#endif // !SCENEMANAGER_H
