#ifndef  SCENE_H
#define SCENE_H

#include "SfmlWindow.h"
using namespace std;

class Scene {
protected :	
	SfmlWindow *sfmlwind;
	virtual void Start();
	virtual int Update();
public:
	Scene();
	Scene(SfmlWindow *window);
	~Scene();
	int RunScene();
};
#endif // ! SCENE_H
