#include "DinamicEnemy.h"

DinamicEnemy::DinamicEnemy() :movementSpeed(6.0f), movementLength(300), counterWalking(0), direction(0),counter(0),alive(true){
	rect.setSize(sf::Vector2f(25,25));
	rect.setPosition(500,500);
	rect.setFillColor(sf::Color::Blue);
	sprite.setTextureRect(sf::IntRect(0, 0,25, 25));
}

void DinamicEnemy::update(){
	sprite.setPosition(rect.getPosition());
}

void DinamicEnemy::updateMovement(){
	 if (direction == 3){
		rect.move(-movementSpeed+elapsed.asSeconds(), 0);
	}
	else if (direction ==6){
		rect.move(movementSpeed+=elapsed.asSeconds(), 0);
	}
	else{
		// No movement
	}
	counterWalking++;
	if (counterWalking == 2){
		counterWalking = 0;
	}

	counter++;
	if (counter >= movementLength){
		direction = generateRandom(10);
		counter = 0;
	}
}