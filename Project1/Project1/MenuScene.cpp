#include "MenuScene.h"

MenuScene::MenuScene() {}

MenuScene::MenuScene(SfmlWindow *_menuwindow) {
	sfmlwind = _menuwindow;
	Start();
}

MenuScene::~MenuScene() {}

void MenuScene::Start() {
	std::cout << "MENU" << endl;
	SetFontTextandBackGround();
	ShowWeather();
	SetMusic();
}

void MenuScene::ShowWeather() {
	menuRequest.DoHttpRequest("http://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22buenos%20aires%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
	
	if (HttpRequest::GetResult() == "Cloudy") 
		weather = "Cloudy";
	
	else if (HttpRequest::GetResult() == "Sunny")
		weather = "Sunny";
	else if (HttpRequest::GetResult() == "Rainy")
		weather = "Rainy";
	else if (HttpRequest::GetResult() == "Mostly Cloudy")
		weather = "Mostly Cloudy";
	else if (HttpRequest::GetResult() == "Partly Cloudy")
		weather = "Partly Cloudy";
	else 
		weather = "default";
	
}



void MenuScene::SetFontTextandBackGround() {
	backgroundMenuTexture.loadFromFile("Assets/Grilla.png");
	backgroundMenuSprite.setTexture(backgroundMenuTexture);
	Menufont.loadFromFile("Assets/Long_Shot.ttf");
	text1.setFont(Menufont);
	text2.setFont(Menufont);
	text3.setFont(Menufont);
	text4.setFont(Menufont);
	text5.setFont(Menufont);
	text6.setFont(Menufont);
}

void MenuScene::SetMusic() {
	menumusic.openFromFile("Assets/menu.ogg");
	menumusic.play();
	menumusic.setLoop(true);
}

void MenuScene::Draw(){
	//Background
	sfmlwind->window.draw(backgroundMenuSprite);
	//All menu text
	text1.setString("Pick up all the coins without loosing lifes and get to the finish line");
	text1.setCharacterSize(20);
	text1.setFillColor(sf::Color::White);
	text1.setPosition(sfmlwind->window.getPosition().x / 2, sfmlwind->window.getPosition().y);
	sfmlwind->window.draw(text1);

	text2.setString("Press SPACE to begin");
	text2.setCharacterSize(20);
	text2.setFillColor(sf::Color::Green);
	text2.setPosition(sfmlwind->window.getPosition().x / 2, sfmlwind->window.getPosition().y + 30);
	sfmlwind->window.draw(text2);
	
	text3.setString("Press F1 to Credits");
	text3.setCharacterSize(20);
	text3.setFillColor(sf::Color::Yellow);
	text3.setPosition(sfmlwind->window.getPosition().x / 2, sfmlwind->window.getPosition().y + 60);
	sfmlwind->window.draw(text3);

	text4.setString("Press ESC to quit");
	text4.setCharacterSize(20);
	text4.setFillColor(sf::Color::Red);
	text4.setPosition(sfmlwind->window.getPosition().x / 2, sfmlwind->window.getPosition().y+90);
	sfmlwind->window.draw(text4);

	text5.setString("Argentina :   " + weather);
	text5.setCharacterSize(30);
	text5.setOutlineColor(sf::Color::Blue);
	text5.setOutlineThickness(3);
	text5.setFillColor(sf::Color::White);
	text5.setPosition(sfmlwind->window.getPosition().x / 2, sfmlwind->window.getPosition().y + 120);
	sfmlwind->window.draw(text5);


	text6.setString("Last score :   " + gamescene.ReadScore());
	text6.setCharacterSize(30);
	text6.setOutlineColor(sf::Color::Blue);
	text6.setOutlineThickness(2);
	text6.setFillColor(sf::Color::White);
	text6.setPosition(sfmlwind->window.getPosition().x / 2, sfmlwind->window.getPosition().y + 170);
	sfmlwind->window.draw(text6);
}

int MenuScene::Update() {	
	int scene = 0;
	while (sfmlwind->window.pollEvent(sfmlwind->event)){
		switch (sfmlwind->event.type){
		case sf::Event::KeyPressed:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
				scene = 1;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1)) {
				scene = 2;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				sfmlwind->window.close();
			}			
			break;
		case sf::Event::Closed:
			sfmlwind->window.close();
			break;
		default:
			break;
		}
	}
		sfmlwind->window.clear(sf::Color::Black);
		Draw();	
		sfmlwind->clock.restart();
		sfmlwind->window.display();
		return scene;		
}


/*
POCO LIBS NO ESTARAN EN EL PROYECTO ESTATICO
PocoFoundationmd.lib
PocoFoundationmt.lib
PocoJSONd.lib
PocoJSONmd.lib
PocoJSONmt.lib
PocoNetd.lib
PocoNetmd.lib
PocoNetmt.lib

*/