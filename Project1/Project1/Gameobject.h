#ifndef  GAMEOBJECT_H
#define GAMEOBJECT_H
#include <SFML/Graphics.hpp>
class Gameobject{
public:
	sf::RectangleShape rect;
	sf::Sprite sprite;
	sf::Text text;
	Gameobject();
};
#endif // ! GAMEOBJECT_H