#include "PlayerOne.h"

PlayerOne::PlayerOne():movementSpeed(2.0f), TotalCoins(0), counterWalking(0), direction(0),lifes(10){
	rect.setSize(sf::Vector2f(32, 32));
	rect.setPosition(25,10);
	rect.setFillColor(sf::Color::Blue);
}

void PlayerOne::update(){
	sprite.setPosition(rect.getPosition());
}

void PlayerOne::updateMovement(){
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
		rect.move(0, -movementSpeed+elapsed.asSeconds());
		direction = 1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
		rect.move(0, movementSpeed+=elapsed.asSeconds());
		direction = 2;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
		rect.move(-movementSpeed+elapsed.asSeconds(), 0);
		direction = 3;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
		rect.move(movementSpeed+=elapsed.asSeconds(), 0);
		direction = 4;
	}
	counterWalking++;
	if (counterWalking == 2){
		counterWalking = 0;
	}
}