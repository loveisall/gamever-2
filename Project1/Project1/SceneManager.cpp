#include "SceneManager.h"

SceneManager::SceneManager() {
	scenewindow = new SfmlWindow();
	currentScene = _menuScene;
	game = new GameScene(scenewindow);
	menu = new MenuScene(scenewindow);
	credits = new CreditScene(scenewindow);
	RunSceneManager();
}

SceneManager::~SceneManager() {}

void SceneManager::RunSceneManager() {
	while (scenewindow->window.isOpen()){
		switch (currentScene) {
		case _menuScene:
			currentScene = menu->RunScene();
			break;
		case _gameScene:
			currentScene = game->RunScene();
			break;
		case _creditsScene:
			currentScene = credits->RunScene();
			break;
		default:
			break;
		}
	}
}

void SceneManager::GoToMenuScene(){
	currentScene = _menuScene;
}

void SceneManager::GoToGameScene() {
	currentScene = _gameScene;
}

void SceneManager::GoToCreditsScene() {
	currentScene = _creditsScene;
}