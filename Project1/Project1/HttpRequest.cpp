
#include "HttpRequest.h"
#include<iostream>
#include<Poco\URI.h>
#include<Poco\Net\HTTPResponse.h>
#include<Poco\Net\HTTPRequest.h>
#include<Poco\Net\HTTPClientSession.h>
#include<Poco\Net\NetException.h>

#include<Poco\JSON\Parser.h>


	std::string HttpRequest::_result;
	

	HttpRequest::HttpRequest() {}

	HttpRequest::~HttpRequest() {}

	void HttpRequest::DoHttpRequest(const std::string &url) {
		//SE CONECTA A LA URL---------------------------------------------------------------
		Poco::URI uri(url);
		std::string temp;
		Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());

		try {
			Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, uri.getPathAndQuery(), Poco::Net::HTTPMessage::HTTP_1_1);
			session.sendRequest(req);
		}
		catch (Poco::Net::HostNotFoundException ex) {
			std::cout << ex.what() << std::endl;
		}
		catch (Poco::TimeoutException ex) {
			std::cout << ex.what() << std::endl;
		}

		try {
			Poco::Net::HTTPResponse response;
			std::istream &is = session.receiveResponse(response);
			std::cout << "HTTPResponse Status: " << response.getStatus() << " HTTPResponse Reason: " << response.getReason() << std::endl;

			//Copiar el contenido a un string
			while (std::getline(is, temp))
				std::cout << temp << std::endl;
			//
		}
		catch (Poco::Net::InvalidSocketException ex) {
			std::cout << ex.what() << std::endl;
		}
		//-------------------------------------------------------------------------

		//Parseo del Json----------------------------------------------------------
		//std::string s = "{ \"query\":{\"count\":1, \"created\" : \"2016-07-09T21:35:56Z\", \"lang\" : \"es-AR\", \"results\" : {\"channel\":{\"item\":{\"condition\":{\"text\":\"Cloudy\"}}}}} }";
		if (!temp.empty()) {
			Poco::JSON::Parser parser;
			Poco::Dynamic::Var result = parser.parse(temp);
			Poco::JSON::Object::Ptr jsonObj = result.extract<Poco::JSON::Object::Ptr>();
			Poco::Dynamic::Var var = jsonObj->get("query");

			jsonObj = var.extract<Poco::JSON::Object::Ptr>();
			var = jsonObj->get("results");

			jsonObj = var.extract<Poco::JSON::Object::Ptr>();
			var = jsonObj->get("channel");

			jsonObj = var.extract<Poco::JSON::Object::Ptr>();
			var = jsonObj->get("item");

			jsonObj = var.extract<Poco::JSON::Object::Ptr>();
			var = jsonObj->get("condition");

			jsonObj = var.extract<Poco::JSON::Object::Ptr>();
			var = jsonObj->get("text");

			_result = var.convert<std::string>();
			std::cout << "Weather: " << _result << std::endl;
		}
		//-------------------------------------------------------------------------
	}

	const std::string &HttpRequest::GetResult() {
		return _result;
	}

	