#ifndef GAMESCENE_H
#define GAMESCENE_H
//SFML INCLUDES ----------------------------------------------------------------------------------------------------------------------------------
#include <SFML/Graphics.hpp>
#include <SFML\Audio\Music.hpp>
#include <SFML\Audio\Sound.hpp>
#include <SFML\Audio\SoundBuffer.hpp>
//GAME INCLUDES ----------------------------------------------------------------------------------------------------------------------------------
#include "PlayerOne.h"
#include "StaticEnemy.h"
#include "HttpRequest.h"
#include "Rnd.h"
#include "DinamicEnemy.h"
#include "Coin.h"
#include "SfmlWindow.h"
#include "Scene.h"
#include <fstream>

//------------------------------------------------------------------------------------------------------------------------------------------------
class GameScene :public Scene{
private:
	void Start()override;
	int Update()override;
	int counter=0;
	int scene;
	int MaxDinamicEnemyQuantity=75;
	int MaxStaticEnemyQuantity=10;
	int MaxCoinQuantity=250;

	//Texturas
	sf::Texture backgroundTexture;
	sf::Texture finishTexture;
	sf::Texture texturePlayer;
	sf::Texture textureDinamicEnemy;//enemigo dinamico
	sf::Texture textureStaticEnemy;//enemigo estatico
	sf::Texture textureCoin;//Textura de la moneda
	//Sprites
	sf::Sprite backgroundSprite;
	sf::Sprite finishSprite;
	//Player Stuff
	PlayerOne player1;
	//Dinamic Enemy Stuff
	vector<DinamicEnemy>::const_iterator iter1;
	vector<DinamicEnemy> enemyArray;
	DinamicEnemy* enemy1 = new DinamicEnemy();
	//Static Enemy Stuff
	vector<StaticEnemy>::const_iterator iter2;
	vector<StaticEnemy> staticenemyArray;
	StaticEnemy *enemy2 = new StaticEnemy();
	//Coin Stuff
	vector<Coin>::const_iterator iter3;
	vector<Coin> CoinArray;
	Coin *coin = new Coin();
	//All In Game Texts
	sf::Text text;
	sf::Text text2;
	//Fonts
	sf::Font font;
	//Audio
	sf::Sound sound1;//Collide with enemy
	sf::Sound sound2;//Collide with coin
	sf::SoundBuffer buff1;
	sf::SoundBuffer buff2;

	void CreateStaticEnemies();
	void CreateDinamicEnemies();
	void CreateCoins();
	void SetTextures();
	void SetFonts();
	void SetText();
	void SetSprites();
	void SetSounds();
	void Collisions();
	void DeletingCollision();
	void KillPlayer();
	void Draw();
	void drawText();
public :
	GameScene();
	GameScene(SfmlWindow *_gamewindow);
	~GameScene();
	void SaveScore();
	string ReadScore();
};
#endif 
