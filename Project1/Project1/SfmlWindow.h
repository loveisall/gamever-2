#ifndef SFMLWINDOW_H
#define SFMLWINDOW_H

#include <SFML/Graphics.hpp>

class SfmlWindow {
public:
	int width;
	int height;
	SfmlWindow();
	~SfmlWindow();
	sf::RenderWindow window;
	sf::Event event;
	sf::Clock clock;//Utilizar despues 
};
#endif // !SFMLWINDOW_H
