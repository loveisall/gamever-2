#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H
#include<SFML\System\NonCopyable.hpp>
#include<string>
	class HttpRequest : private sf::NonCopyable {
	public:
		HttpRequest();
		~HttpRequest();
		static std::string _result;	
		static void DoHttpRequest(const std::string &url);
		static const std::string &GetResult();
	};

#endif //HTTP_REQUEST_H